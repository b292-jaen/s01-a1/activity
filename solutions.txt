S01 Activity
1. List of Books Authored by Marjorie Green.
- You Can Combat Computer Stress!
- The Busy Executive's Database Guide

2. List of Books Authored by Michael O'Leary.
- Cooking with Computers

3. Author/s of "The Busy Excecutive's Database Guide"
- Marjorie Green
- Bennet Abraham

4. Publisher of "But Is It User Friendly?".
- Algodata Infosystems

5. Books Published by Algodata Infosystems.
- The Busy Executive's Database Guide
- Cooking with Computers
- Straight Talk About Computers
- But Is It User Friendly?
- Secrets of Silicon Valley
- Net Etiquette